﻿@extends('layouts.app')
@section('content')

<div class="clearfix"></div>

<div class="mstslider">

<!-- masterslider -->
<div class="master-slider ms-skin-default" id="masterslider">

   <div class="ms-slide slide-1" data-delay="7">
         
        <!-- slide background -->
        <img src="images/bg-parallax-6.jpg" alt=""/>     
                
        <div class="ms-layer centext text1 white"
            style="top:320px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="300"
            data-ease="easeOutExpo"
        ><strong>Your Right-Shore Development Partner</strong></div>
        
       
    </div><!-- end slide -->

</div><!-- end of masterslider -->

</div><!-- end slider -->


<div class="clearfix"></div>

<div class="features_sec45">
<div class="container">
    
    <div class="title2">
        <h2><span class="line"></span><span class="text">What We do</span></h2>
    </div>
    
    <h1><em>Your Concept to Realization</em></h1>
    

    <p class="less2">We just don't codify, we Deliver Working Applications as per your suit<br/>We understand the concepts behind your thoughts and help you with both roadmapping as well as execution of your projects.</p>
    
</div>

</div><!-- end section -->
     

<div class="clearfix"></div>

<div class="features_sec2">
<div class="container">

	<div class="title1">
    	<h2><span class="line"></span><span class="text">WHY US?</span><em></em></h2>
    </div>
    
    <div class="clearfix margin_top2"></div>
    
<div class="one_third">
    
    	<div class="box">
        	
            <img src="images/quality.png" alt="" />
            
            <h5>Quality</h5>
            <p>Company culture built on the principals of Right Quality at First Attempt</p>
            
        </div>
        
    </div><!-- end section -->
    
    <div class="one_third">
    
    	<div class="box">
        	
            <img src="images/home/technology.jpg" alt="" />
            
            <h5>E.R.P. (MahaErp)</h5>
            <p>Abstract of an opensource E.R.P to meet the needs of various industries</p>
            
        </div>
        
    </div><!-- end section -->
    
	<div class="one_third last">
    
    	<div class="box">
        	
            <img src="images/scalable-solutions.png" alt="" />
            
            <h5>Scalable Solutions</h5>
            <p>Scalability to fast growth is in DNA of our solutions</p>
            
        </div>
        
    </div><!-- end section -->

        <div class="clearfix margin_top2"></div>

	<div class="one_half" >
    
        <div class="box1">
            
            <img src="images/ios-android.png" alt="" />
            
            <h5>Mobile Applications</h5>
            <p>Mobile Applications with best in class quality and framework integration to give larger-than-life experience for your business needs</p>
            
        </div>
        
    </div>
    
    <div class="one_half last">
    
        <div class="box2">
            
            <img src="images/custome-development.png" alt="" />
            
            <h5>Custom Applications</h5>
            <p>Idea Incubation from scratch or restructuring, We understand your vision and ensure our solutions harmonize them</p>
            
        </div>
        
    </div>
   

</div>
</div>

<div class="features_sec32">
<div class="container">

	<div class="title2">
    	<h2><span class="line"></span><span class="text">What You Get</span><em>On Time Right Quality working Applications</em></h2>
    </div>
    
    <div class="clearfix margin_top3"></div>
    
    <div class="one_third">
    
        <div class="box">


             <img src="images/expert-team-icon.png" alt="">
            
            <div class="right">
                <h5>Tech-Savvy Team</h5>
                <p>Developers of your team are Tech-Savvy as well as Result Oriented to meet your business suit</p>
            </div>

        </div><!-- end section -->
        
    </div><!-- end all sections -->
    
    <div class="one_third">
    
        <div class="box">

             <img src="images/proactive-communication.png" alt="">
            
            <div class="right">
                <h5>Pro-active communications</h5>
                <p>Communication: A key to success of any project, is the major winning point for us. We ensure very transparent commuincation between both Teams</p>
            </div>

        </div><!-- end section -->
          
    </div><!-- end all sections -->
    
    <div class="one_third last">
    
        <div class="box">
        
            <img src="images/accuracy.png" alt="">
            
            <div class="right">
                <h5>100 % accuracy</h5>
                <p>Crisp and clear Communication combined with highly skilled team makes a project 100% Successful</p>
            </div>
		
        </div><!-- end section -->
           
    </div><!-- end all sections -->

</div>
</div>

<div class="clearfix"></div>

<!-- div class="features_sec38">
<div class="container">
    
    <div class="flexslider carousel">
        <ul class="slides">
        
            <li>
                
                <img src="http://placehold.it/90x90" alt="" />
                <h6>Austin Lisandro <em>- websitename -</em></h6>
                
                <p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>
            
            </li>
            
            <li>
                
                <img src="http://placehold.it/90x90" alt="" />
                <h6>Alana Kasandra <em>- websitename -</em></h6>
                
                <p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>
            
            </li>
            
            <li>
                
                <img src="http://placehold.it/90x90" alt="" />
                <h6>Jorge Anthony <em>- websitename -</em></h6>
                
                <p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>
            
            </li>
            
            <li>
                
                <img src="http://placehold.it/90x90" alt="" />
                <h6>Fernanda Elyse <em>- websitename -</em></h6>
                
                <p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>
            
            </li>
            
        </ul>
    </div>

</div>
</div --><!-- end features section 38 -->

<div class="clearfix"></div>

<div class="parallax_section3">
<div class="container">
    
    <div class="one_full stcode_title6 white">
        <h2 class="white">We are Experts at</h2>
        <br>
    </div>
    
    <div class="piechart3"><canvas class="loader001" width="292" height="292"></canvas> <br> IT INCUBATION CENTER </div>
    <div class="piechart3"><canvas class="loader002" width="292" height="292"></canvas> <br> CUSTOM WEB APPLICATIONS </div>
    <div class="piechart3"><canvas class="loader003" width="292" height="292"></canvas> <br> MOBILE APPLICATIONS </div>
    <div class="piechart3"><canvas class="loader004" width="292" height="292"></canvas> <br> ERP (MahaErp) -- Opensource abstract </div>
    
    <script>
    (function($) {
    "use strict";
    
    $(document).ready(function() {
        $('.loader001').ClassyLoader3({
            percentage: 90,
            speed: 30,
            fontSize: '50px',
            diameter: 100,
            lineColor: '#ff9819',
            remainingLineColor: 'rgba(255,255,255,0.3)',
            lineWidth: 10
        });
    });

    $(document).ready(function() {
        $('.loader002').ClassyLoader3({
            percentage: 100,
            speed: 30,
            fontSize: '50px',
            diameter: 100,
            lineColor: '#ff9819',
            remainingLineColor: 'rgba(255,255,255,0.3)',
            lineWidth: 10
        });
    });
    
    $(document).ready(function() {
        $('.loader003').ClassyLoader3({
            percentage: 100,
            speed: 30,
            fontSize: '50px',
            diameter: 100,
            lineColor: '#ff9819',
            remainingLineColor: 'rgba(255,255,255,0.3)',
            lineWidth: 10
        });
    });
    
    $(document).ready(function() {
        $('.loader004').ClassyLoader3({
            percentage: 75,
            speed: 30,
            fontSize: '50px',
            diameter: 100,
            lineColor: '#ff9819',
            remainingLineColor: 'rgba(255,255,255,0.3)',
            lineWidth: 10
        });
    });


    })(jQuery);
    </script><!-- end section -->
    

</div>
</div>

<div class="clearfix"></div>

<div class="container">

    <div class="divider_line9 lessm2"></div><div class="clearfix"></div>
    
    <div class="stcode_title8">
    
        <h2><span class="line"></span><span class="text">Our <strong>Clients</strong><span></span></span></h2>
    
    </div><!-- end section title heading -->
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/rsbl.jpg" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/heminfotech.png" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/hi-will-logo.jpg" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4 active">
        
            <div class="flipscont4">
                <img src="images/clients/impetix.png" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/genesis.jpg" alt="">
            </div>
            
            
        </div>
    
    </div><!-- end section -->
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/pratrikshainterior.png" alt="">
            </div>
            
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/studioroopkala.png" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/setutech1.png" alt="">
            </div>
           
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/welcomecineplex.jpg" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/grecelllogo.png" alt="">
            </div>
            
           
        </div>
    
    </div><!-- end section -->

</div>

<div class="clearfix marb12"></div>

<div class="clearfix"></div>
@stop