<header id="header">

	<!-- Top header bar -->
	<div id="topHeader">
    
	<div class="wrapper">
         
        <div class="top_nav">
        <div class="container">
        	
            <div class="left">
            
            	<ul class="topsocial">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            
            </div><!-- end left links -->
            
            <div class="right">
                
                Call Us: <strong>+91 966 254 7607</strong>       Email: <a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a>
          
        </div><!-- end right links -->
        
        </div>
        </div>
            
 	</div>
    
	</div><!-- end top navigations -->
	
    
	<div id="trueHeader">
    
	<div class="wrapper">
    
     <div class="container">
    
		<!-- Logo -->
		<div class="logo"><a href="index.html" id="logo"></a></div>
		
	<!-- Navigation Menu -->
	<nav class="menu_main">
        
	<div class="navbar yamm navbar-default">
    
    <div class="container">
      <div class="navbar-header">
        <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  > <span>Menu</span>
          <button type="button" > <i class="fa fa-bars"></i></button>
        </div>
      </div>
      
      <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
      
        <ul class="nav navbar-nav">
        
        <li class="dropdown"><a href="index.html" data-toggle="dropdown" class="dropdown-toggle active">Home</a>
        	<ul class="dropdown-menu multilevel" role="menu">
                <li><a href="index.html" class="active">Header Style 1</a></li>
                <li><a href="index-2.html">Header Style 2</a></li>
                <li><a href="index-3.html">Header Style 3</a></li>
                <li><a href="index-4.html">Header Style 4</a></li>
         	</ul>
        </li>
               
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">About Firm</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="about.html">About Page Style 1</a></li>
                <li><a href="about-2.html">About Page Style 2</a></li>
            </ul>
        </li>
        
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Attorneys</a>
        	<ul class="dropdown-menu multilevel" role="menu">
                <li><a href="attorneys.html">Attorneys Style 1</a></li>
                <li><a href="attorneys-2.html">Attorneys Style 2</a></li>
                <li><a href="attorneys-3.html">Attorneys Style 3</a></li>
                <li><a href="attorneys-4.html">Attorneys Style 4</a></li>
                <li><a href="attorneys-fullbio.html">Attorneys Full BIO</a></li>
            </ul>
        </li>
        
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Practice Areas</a>
        	<ul class="dropdown-menu multilevel" role="menu">
                <li><a href="practice.html">Practice Areas Style 1</a></li>
                <li><a href="practice-2.html">Practice Areas Style 2</a></li>
                <li><a href="practice-3.html">Practice Areas Style 3</a></li>
                <li><a href="practice-4.html">Practice Areas Style 4</a></li>
            </ul>
        </li>
        
        <li class="dropdown yamm-fw"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">Features</a>
        <ul class="dropdown-menu">
          <li> 
            <div class="yamm-content">
              <div class="row">
              
                <ul class="col-sm-6 col-md-4 list-unstyled two">
                    <li><a href="template1.html"><i class="fa fa-plus-square"></i> Accordion &amp; Toggle</a></li>
                    <li><a href="template2.html"><i class="fa fa-leaf"></i> Title Styles</a></li>
                    <li><a href="template3.html"><i class="fa fa-bars"></i> List of Dividers</a></li>
                    <li><a href="template4.html"><i class="fa fa-exclamation-triangle"></i> Boxes Alert</a></li>
                    <li><a href="template5.html"><i class="fa fa-hand-o-up"></i> List of Buttons</a></li>
                    <li><a href="template6.html"><i class="fa fa-cog"></i> Carousel Sliders</a></li>
                    <li><a href="template7.html"><i class="fa fa-file-text"></i> Page Columns</a></li>
                    <li><a href="template8.html"><i class="fa fa-rocket"></i> Animated Counters</a></li>
                    <li><a href="template17.html"><i class="fa fa-question"></i> Faqs Page</a></li>
                </ul>
                
                <ul class="col-sm-6 col-md-4 list-unstyled two">
                    <li><a href="template9.html"><i class="fa fa-pie-chart"></i> Pie Charts</a></li>
                    <li><a href="template10.html"><i class="fa fa-flag"></i> Font Icons</a></li>
                    <li><a href="template11.html"><i class="fa fa-umbrella"></i> Flip Boxes</a></li>
                    <li><a href="template12.html"><i class="fa fa-picture-o"></i> Image Frames</a></li>
                    <li><a href="template13.html"><i class="fa fa-table"></i> Pricing Tables</a></li>
                    <li><a href="template14.html"><i class="fa fa-line-chart"></i> Progress Bars</a></li>
                    <li><a href="template15.html"><i class="fa fa-toggle-on"></i> List of Tabs</a></li>
                    <li><a href="template16.html"><i class="fa fa-paper-plane"></i> Popover &amp; Tooltip</a></li>
                    <li><a href="template18.html"><i class="fa fa-play-circle"></i> Video Backgrounds</a></li>
                </ul>
                
                <ul class="col-sm-6 col-md-4 list-unstyled two">
                	<li>
                    	<p>About the Website</p>
                    </li>
                    <li class="dart">
                    	<img src="images/menu-img.jpg" alt="" class="menuimg" />
                        There are many variations of passages Lorem <strong>Ipsum available</strong> but the majority have suffered alteration in some form, by injected humour, or randomised words if you are going to use a passage.
                    </li>
                </ul>
                
                
              </div>
            </div>
          </li>
        </ul>
        </li>
        
        <li class="dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">Blog </a>
        <ul class="dropdown-menu multilevel" role="menu">
        	<li> <a href="blog-4.html">With Masonry</a> </li>
            <li> <a href="blog.html">With Large Image</a> </li>
            <li> <a href="blog-2.html">With Medium Image</a> </li>
            <li> <a href="blog-3.html">With Small Image</a> </li>
            <li> <a href="blog-post.html">Single Post</a> </li>
        </ul>
        </li>
        
        <li class="dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">Contact </a>
        <ul class="dropdown-menu two" role="menu">
          <li> <a href="contact.html">Contact Style 1</a> </li>
          <li> <a href="contact2.html">Contact Style 2</a> </li>
          <li> <a href="contact3.html">Contact Style 3</a> </li>
        </ul>
        </li>
        
        </ul>
        
      </div>
      </div>
     </div>
     
	</nav><!-- end Navigation Menu -->
        
	</div>
		
	</div>
    
	</div>
    
</header>