<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>

 @include('includes.head')
    
</head>

<body>

<div class="site_wrapper">

<div class="clearfix"></div>

<div class="top_nav two">

    <div class="container">
        
        <div class="left">
        
			 <a href="mailto:info@yourwebsite.com"><i class="fa fa-envelope"></i>&nbsp; info@mahavirconsultants.com</a>
            
        </div><!-- end left -->
        
        <div class="right">
        
            <a href="tel://+91-9869-995-871"><i class="fa fa-phone-square"></i>+91-9869-995-871</a>
            <!-- i class="fa fa-phone-square"></i>&nbsp; +88 123 456 7890
            <i class="fa fa-phone-square"></i>&nbsp; +88 123 456 7890 -->
            
        </div><!-- end right -->
        
    </div><!-- end top links -->

</div>

<div class="clearfix"></div>

<header class="header">
 
 @include('includes.header')
    
</header>

@yield('content')

</div><!-- end slider -->

<div class="clearfix"></div>

<footer class="footer">

@include('includes.footer')

</footer>

</div>
  
@include('includes.js')

</body>
</html>