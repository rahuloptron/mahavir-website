<footer>

<div class="footer">

    <div class="container">
    
        <div class="left">
        
            <h5>Get Free Consultation</h5>
            <h6>Available 24/7</h6>
            <h3>+91 966 254 7607</h3>
            
        </div><!-- end section -->
        
        <div class="center">
        
            <h5>Message Us Now</h5>
            <h6>Available 24/7</h6>
            <h3><a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a></h3>
        
        </div><!-- end section -->
        
        <div class="right">
        
            <h5>Address Location</h5>
            <h6>315, Devnanadan Mall, Opp. Sansyas Ashram, Ellisbridge, Ahmedabad, Gujarat 380006 <br /> <a href="#">View Map</a></h6>


            
        
        </div><!-- end section -->
    
    </div>
    
</div><!-- end footer -->

<div class="copyrights">
<div class="container">

	<div class="one_half">Copyright © 2018 Hem Infotech. All Rights Reserved</div>
	
    <div class="one_half last"><a href="#">Notices</a> | <a href="#">Privacy Policy</a> | <a href="#">Careers</a></div>
    
    
</div>
</div><!-- end copyrights -->

</footer>