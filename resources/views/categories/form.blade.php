<div class="form-group {{ $errors->has('categoryName') ? 'has-error' : ''}}">
    {!! Form::label('categoryName', 'Categoryname', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('categoryName', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('categoryName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('subCategories') ? 'has-error' : ''}}">
    {!! Form::label('subCategories', 'subCategories', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('subCategories', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('subCategories', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
