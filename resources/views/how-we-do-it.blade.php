@extends('layouts.app')
@section('content')

<div class="clearfix"></div>

<div class="parallax_section8">
<div class="container">

    <h1 class="white">Partner selects his team</h1>
    
  
</div>
</div><!-- end features section 11 -->

<div class="clearfix"></div>

<div class="features_sec2">
<div class="container">

	<div class="title1">
    	<h2><span class="line"></span><span class="text">HOW DOES OUR PROCESS WORK?</span><em>At your Service always</em></h2>
    </div>
    
    <div class="clearfix margin_top2"></div>
    <!-- 
	<div class="one_third">
    
    	<div class="box">
        	
            <img src="http://placehold.it/100x100" alt="" />
            
            <h5>Selection</h5>
            <p>Exact original form accompan by English versions treatise on the theory of ethics will many covers web sites.</p>
            
        </div>
        
    </div>
    
    <div class="one_third">
    
    	<div class="box">
        	
            <img src="http://placehold.it/100x100" alt="" />
            
            <h5>Understanding delivery</h5>
            <p>Exact original form accompan by English versions treatise on the theory of ethics will many covers web sites.</p>
            
        </div>
        
    </div>
    
	<div class="one_third last">
    
    	<div class="box">
        	
            <img src="http://placehold.it/100x100" alt="" />
            
            <h5>Execution</h5>
            <p>Exact original form accompan by English versions treatise on the theory of ethics will many covers web sites.</p>
            
        </div>
        
    </div>

     <div class="clearfix margin_top2"></div>
    
	<div class="one_third">
    
        <div class="box">
            
            <img src="http://placehold.it/100x100" alt="" />
            
            <h5>Delivery</h5>
            <p>Exact original form accompan by English versions treatise on the theory of ethics will many covers web sites.</p>
            
        </div>
        
    </div>
    
    <div class="one_third">
    
        <div class="box">
            
            <img src="http://placehold.it/100x100" alt="" />
            
            <h5>New Project</h5>
            <p>Exact original form accompan by English versions treatise on the theory of ethics will many covers web sites.</p>
            
        </div>
        
    </div>
    
    <div class="one_third last">
    
        <div class="box">
            
            <img src="http://placehold.it/100x100" alt="" />
            
            <h5>YOU SCALE</h5>
            <p>Exact original form accompan by English versions treatise on the theory of ethics will many covers web sites.</p>
            
        </div>
        
    </div>

	-->
	<div class="one_half">
<!-- 		
		BEFORE WE START ?
		<br/>
		1. Has a project work-plan/delivery-plan for the entire project been created?
		<br/>
		2. What project management procedures will the dev/design company use to control the project?
		<br/>
		3. Who will be the key members from the dev/design company in the project and what will be their roles and responsibilities?
				Is the dev/design company clear on what resources they need from us and when they will be needed?
		<br/>
		4. What in your opinion are the key risk factors that need to be mitigated or taken into account before the project begins and how will you mitigate/address those as the project kicks off?
		<br/>
		5. How do you quality benchmark the deliverables before they are sent to us for User Acceptance Testing (UAT)?
		<br/>
		6. How do you tackle new change requests or change in expectations mid-way the project?
		<br/>
		7. What is the procedure/process through which you intake, assess, evaluate and incorporate feedback into your deliverables?
		<br/>
		8. What are your procedures to ensure that the project does not trip into a direction it isn�t intended to? What is your Plan-B if it is discovered that it accidentally has.
		<br/> --> 
		<img src="images/home/scientific-methodology.png" alt="" />
	</div>	
	<div class="one_half last">
		<!-- AFTER WE CLOSE ?
		
		<br/>
		1. Have the deliverables specified in the project definition been completed up to this point?
		<br/>
		2. Which are the key items completed, pending and under-progress at this point that will be included in the next iteration and which will be deferred to the future?
		<br/>
		3. Have the appropriate deliverables been agreed to and approved by us?
		<br/>
		4. Can the Product Owner from the dev/design team clearly explain where the project is vs. where it should be at this time?
		<br/>
		5. Is the last deliverable market ready?
			A. What can I expect to see in the next deliverable or at the end of next iteration?
			B. Are issues being resolved in a timely manner?
			C. Are scope-change requests being managed, and are we formally approving changes?
			D. Are risks being identified and managed successfully?
			e. Should the contract or project definition be updated to reflect any major changes to the project?
		<br/> -->
		<img src="images/home/software_dev_sdlc.png" alt="" />
	</div>
</div>
</div>

<div class="clearfix"></div>
<div  class="mobile-hide">
<div class="pritable">
<div class="container">

	<div class="title2">
    	<h2><span class="line"></span><span class="text">Get Your Package</span></h2>
    </div>
    
    <div class="clearfix margin_top7"></div>
    
    <div class="pacdetails">
    	
        <div class="title">

            <h2>Highlights</h2>
        	
		</div>
        
        <ul>
            <li><strong>Time to get right developers</strong></li>
            <li><strong>Time to start a project </strong></li>
            <li><strong>Acquisition cost</strong></li>
            <li><strong>Recurring cost of training & benefits</strong></li>
            <li><strong>Time to scale size of team  </strong></li>
            <li><strong>Pricing (weekly Average) </strong></li>
            <li><strong>Project failure risk</strong></li>
            <li><strong>Developers backed by a delivery team</strong></li>
            <li><strong>Dedicated resources</strong></li>
            <li><strong>Agile Development methodology</strong></li>
            <li><strong>Quality guarantee</strong></li>
            <li><strong>Impact due to turnover </strong></li>
            <li><strong>Structured training programs</strong></li>
            <li><strong>Communications</strong></li>
            <li><strong>Termination costs</strong></li>
            <li><strong>Assured work rigor</strong></li>
            <li><strong>Tools and professional environment</strong></li>
        </ul>
      
    </div><!-- end section -->
    
    <div class="pacdetails two">
    	
        <div class="title">
        	<h2>Mahavir Consultants</h2>
         
		</div>
        
        <ul>
            <li><strong>1 day - 2 weeks</strong></li>
            <li><strong>1 day - 2 weeks </strong></li>
            <li><strong>0</strong></li>
            <li><strong>0</strong></li>
            <li><strong>1 day - 2 weeks</strong></li>
            <li><strong>$600 - $1,299</strong></li>
            <li><strong>Extremely Low</strong></li>
            <li><strong>YES</strong></li>
            <li><strong>YES</strong></li>
            <li><strong>YES</strong></li>
            <li><strong>YES</strong></li>
            <li><strong>NONE</strong></li>
            <li><strong>YES</strong></li>
            <li><strong>Seamless</strong></li>
            <li><strong>CONDITIONAL</strong></li>
            <li><strong>40 hrs/week</strong></li>
            <li><strong>Best-in-Class</strong></li>
        </ul>
    
    </div><!-- end section -->
    
    <div class="pacdetails three">
    	
        <div class="title">
        	
        	<h2>Free Lancers</h2>
          
		</div>
        
        <ul>
            <li><strong>1 day - 2 weeks</strong></li>
            <li><strong>1 day - 2 weeks </strong></li>
            <li><strong>0</strong></li>
            <li><strong>0</strong></li>
            <li><strong>1 day - 2 weeks</strong></li>
            <li><strong>$400 - $1,299</strong></li>
            <li><strong>Extremely High</strong></li>
            <li><strong>NO</strong></li>
            <li><strong>YES</strong></li>
            <li><strong>NO</strong></li>
            <li><strong>NO</strong></li>
            <li><strong>HIGH</strong></li>
            <li><strong>NO</strong></li>
            <li><strong>Seamless</strong></li>
            <li><strong>NONE</strong></li>
            <li><strong>40 hrs/week</strong></li>
            <li><strong>Conditional</strong></li>
        </ul>
    
    </div><!-- end section -->
    
    

</div>
</div><!-- end pricing details -->
</div>

<div class="clearfix"></div>

<!-- div class="features_sec38">
<div class="container">
    
    <div class="flexslider carousel">
        <ul class="slides">
        
            <li>
                
                <img src="http://placehold.it/90x90" alt="" />
                <h6>Austin Lisandro <em>- websitename -</em></h6>
                
                <p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>
            
            </li>
            
            <li>
                
                <img src="http://placehold.it/90x90" alt="" />
                <h6>Alana Kasandra <em>- websitename -</em></h6>
                
                <p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>
            
            </li>
            
            <li>
                
                <img src="http://placehold.it/90x90" alt="" />
                <h6>Jorge Anthony <em>- websitename -</em></h6>
                
                <p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>
            
            </li>
            
            <li>
                
                <img src="http://placehold.it/90x90" alt="" />
                <h6>Fernanda Elyse <em>- websitename -</em></h6>
                
                <p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>
            
            </li>
              
        </ul>
    </div>

</div>
</div --><!-- end features section 38 -->

<div class="clearfix"></div>

<div class="container">

    <div class="divider_line9 lessm2"></div><div class="clearfix"></div>
    
    <div class="stcode_title8">
    
        <h2><span class="line"></span><span class="text">Our <strong>Clients</strong><span></span></span></h2>
    
    </div><!-- end section title heading -->
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/rsbl.jpg" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/heminfotech.png" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/hi-will-logo.jpg" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4 active">
        
            <div class="flipscont4">
                <img src="images/clients/impetix.png" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/genesis.jpg" alt="">
            </div>
            
            
        </div>
    
    </div><!-- end section -->
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/pratrikshainterior.png" alt="">
            </div>
            
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/studioroopkala.png" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/setutech1.png" alt="">
            </div>
           
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/welcomecineplex.jpg" alt="">
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/grecelllogo.png" alt="">
            </div>
            
           
        </div>
    
    </div><!-- end section -->

</div>

<div class="clearfix marb12"></div>

<div class="clearfix"></div>
@stop