    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- animations -->
<script src="js/animations/js/animations.min.js" type="text/javascript"></script>

<!-- slide panel -->
<script type="text/javascript" src="js/slidepanel/slidepanel.js"></script>

<!-- mega menu -->
<script src="js/mainmenu/bootstrap.min.js"></script> 
<script src="js/mainmenu/customeUI.js"></script> 

<!-- MasterSlider -->
<script src="js/masterslider/jquery.easing.min.js"></script>
<script src="js/masterslider/masterslider.min.js"></script>

<script type="text/javascript">
(function($) {
 "use strict";

    var slider = new MasterSlider();
    // adds Arrows navigation control to the slider.
   
     slider.setup('masterslider' , {
         width:1400,    // slider standard width
         height:650,   // slider standard height
         space:0,
         speed:45,
         layout:'fullwidth',
         loop:true,
         preload:0,
         autoplay:true,
         view:"fade"
    });
    
    var slider2 = new MasterSlider();

     slider2.setup('masterslider2' , {
         width:1400,    // slider standard width
         height:520,   // slider standard height
         space:0,
         speed:45,
         layout:'fullwidth',
         loop:true,
         preload:0,
         autoplay:true,
         view:"basic"
    });
    
})(jQuery);
</script>


<!-- scroll up -->
<script src="js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- sticky menu -->
<script type="text/javascript" src="js/mainmenu/sticky-6.js"></script>
<script type="text/javascript" src="js/mainmenu/modernizr.custom.75180.js"></script>


<!-- flexslider -->
<script defer src="js/flexslider/jquery.flexslider.js"></script>


<!-- owl carousel -->
<script src="js/carouselowl/owl.carousel.js"></script>

<!-- basic slider -->
<script type="text/javascript" src="js/basicslider/bacslider.js"></script>
<script type="text/javascript">
(function($) {
 "use strict";
 
    $(document).ready(function() {
        $(".main-slider-container").sliderbac();
    });
    
})(jQuery);
</script>

<!-- tabs -->
<script src="js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>

<!-- Accordion-->
<script type="text/javascript" src="js/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="js/accordion/custom.js"></script>

<script type="text/javascript">
$(function () {

var arr = @json($companyRecord);

$('#btnInsert').click(function () {

var string = $('#companyname').val();

var str = string.toLowerCase();

var stats = $.inArray(str, arr);
if (stats == -1) {
arr.push(str);
} else {

 document.getElementById("usernameError").innerHTML = "Company Already Exist";

return false;
}
});
})
</script>

