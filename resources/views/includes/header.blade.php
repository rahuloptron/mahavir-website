  <div class="container">
    
    <!-- Logo -->
    <div class="logo"><a href="index.html" id="logo"></a></div>
    
  <!-- Navigation Menu -->
  <nav class="menu_main">
        
  <div class="navbar yamm navbar-default">
    
    <div class="container">
      <div class="navbar-header">
        <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  > <span>Menu</span>
          <button type="button" > <i class="fa fa-bars"></i></button>
        </div>
      </div>
      
      <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
      
        <ul class="nav navbar-nav">

          <li class="dropdown yamm-fw"><a href="index.html">Home</a></li>
        
        <li class="dropdown yamm-fw"><a href="what-we-do.html">What we do</a></li>

         <li class="dropdown yamm-fw"><a href="how-we-do-it.html">How we do</a></li>

        
           <li class="dropdown yamm-fw"><a href="which-technologies.html">Which Technologies</a></li>

            <li class="dropdown yamm-fw"><a href="who-we-are.html">Who we are</a></li>

            <li class="dropdown yamm-fw"><a href="contact.html">Contact Us</a></li>

            <li class="dropdown yamm-fw"><a href="crm">CRM</a></li>
                
        </ul>
        
      </div>
      </div>
     </div>
     
  </nav><!-- end Navigation Menu -->
    
  </div>
    
