<title> </title>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />

     <meta name="csrf-token" content="{{ csrf_token() }}">

     
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    
    <!-- Favicon --> 
    <link rel="shortcut icon" href="images/favicon.png">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
	
	<link href="http://optrondigital.mahavirconsultants.com/web/css/im_livechat.external_lib" rel="stylesheet"/>
	<script src="http://optrondigital.mahavirconsultants.com/web/js/im_livechat.external_lib" type="text/javascript"></script>
	<script src="http://optrondigital.mahavirconsultants.com/im_livechat/loader/optrondigital/3" type="text/javascript"></script>
    
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <!-- font awesome icons -->
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    
    <!-- simple line icons -->
    <link rel="stylesheet" type="text/css" href="css/simpleline-icons/simple-line-icons.css" media="screen" />
    
    <!-- animations -->
    <link href="js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
    <!-- responsive devices styles -->
    <link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    
    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="css/shortcodes.css" type="text/css" /> 
    

    <link rel="stylesheet" href="css/colors/orange.css" />

    
    <!-- mega menu -->
    <link href="js/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="js/mainmenu/menu-6.css" rel="stylesheet">
    
    <!-- slide panel -->
    <link rel="stylesheet" type="text/css" href="js/slidepanel/slidepanel.css">
    
    <!-- MasterSlider -->
    <link rel="stylesheet" href="js/masterslider/style/masterslider.css" />
    <link rel="stylesheet" href="js/masterslider/skins/default/style.css" />
    
    <!-- owl carousel -->
    <link href="js/carouselowl/owl.transitions.css" rel="stylesheet">
    <link href="js/carouselowl/owl.carousel.css" rel="stylesheet">
    
    <!-- icon hover -->
    <link rel="stylesheet" href="js/iconhoverefs/component.css" />
    
    <!-- basic slider -->
    <link rel="stylesheet" href="js/basicslider/bacslider.css" />
    
    <!-- cubeportfolio -->
   <!--  <link rel="stylesheet" type="text/css" href="js/cubeportfolio/cubeportfolio.min.css"> -->
    
    <!-- flexslider -->
    <link rel="stylesheet" href="js/flexslider/flexslider.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="js/flexslider/skin.css" />
    
    <!-- tabs -->
 <!--    <link rel="stylesheet" type="text/css" href="js/tabs/assets/css/responsive-tabs3.css"> -->
    
    <!-- accordion -->
    <link rel="stylesheet" type="text/css" href="js/accordion/style.css" />
    
    <!-- classyloader-->
    <script type="text/javascript" src="js/universal/jquery.js"></script>
    <script src="js/classyloader/jquery.classyloader.min.js"></script>

     <link rel="stylesheet" href="js/form/sky-forms2.css" type="text/css" media="all">


     <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
   
