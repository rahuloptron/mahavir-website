<footer class="footer">

<div class="clearfix"></div>

<div class="container">

    <div class="one_half">
        <ul class="faddress">
            <li><h2 class="white">Mahavir Consultants</h2></li>
            <li><i class="fa fa-map-marker fa-lg"></i>&nbsp; 3, Mahavir Nagar, Shankar Lane, Kandivali (W)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mumbai, 67, India</li>
            <li><i class="fa fa-phone"></i><a href="tel://+91-9869-995871">&nbsp; +91-9869-995871</a></li>
            <!-- li><i class="fa fa-print"></i>&nbsp; 1 -234 -456 -7890</li -->
            <li><a href="mailto:info@mahavirconsultants.com"><i class="fa fa-envelope"></i>&nbsp; info@mahavirconsultants.com</a></li>
            <li><img src="images/footer-wmap.png" alt="" /></li>
        </ul>
    </div><!-- end address -->
    
        
    <div class="one_fourth">
    <div class="siteinfo">
    
        <h4 class="lmb">About Us</h4>
        
        <p>We are new age IT partner for your initiatives</p>
        <br />
        <p>With more than 11+yrs for hard code engineering solution development ranging from core -telecom vas products to web aplications.
		We work closly with out clients to get the best of the multiple scenarios</p>
               
    </div>
    </div><!-- end site info -->
    
 </div><!-- end footer -->

<div class="clearfix"></div>

<div class="copyright_info">
<div class="container">

    <div class="clearfix divider_dashed10"></div>
    
    <div class="one_half">
    
        Copyright © 2018 Mahavir Consultants. All rights reserved.  <a href="#">Terms of Use</a> | <a href="#">Privacy Policy</a>
        
    </div>
    
    <div class="one_half last">
        
        <ul class="footer_social_links">
            <li><a href="https://www.facebook.com/Mahavir-Consultants-1962124537436473"><i class="fa fa-facebook"></i></a></li>
            <!-- li><a href="#"><i class="fa fa-twitter"></i></a></li -->
            <li><a href="https://www.linkedin.com/company/mahavir-consultants/"><i class="fa fa-linkedin"></i></a></li>
           
        </ul>
            
    </div>
    
</div>
</div><!-- end copyright info -->
</footer>