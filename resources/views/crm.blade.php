@extends('layouts.appcrm')
@section('content')



<div class="clearfix"></div>

<div class="page_title">
<div class="container">

    <div class="title"><h1>CRM</h1></div>
        
  
</div>
</div><!-- end page title -->

<div class="clearfix"></div>

<div class="content_fullwidth">

<div class="container">

@if (session('status'))
    <div>
       <h1>{{ session('status') }}</h1> 
    </div>
@endif
	
		<div class="one_half">
      
        <div class="cforms">
		<form method="POST" url="crmController@store" class="sky-form" style="border: 1px solid #000;background-color: #dff2ff;padding-left:15px;padding-right:15px;padding-bottom: 50px;padding-top: 35px;">
         {{ csrf_field() }}
          <header><strong>Create Your Own CRM Service</strong></header>
          <fieldset>	
            <div class="row">
              <section class="col col-4">
                <label class="label">SubDomain</label>
				        <label class="input">
                <input type="text" name="subdomain"  required/>
                </label>
              </section> 
              <section class="col col-8">
                <label class="label">Domain</label>
                <label class="input">
                <input type="text" name="domain" placeholder=".mahavirconsultants.com" readonly/>
                </label>
              </section> 

            </div>

            <div class="row">
               <section class="col col-6">
                <label class="label">Contact Name</label>
                <label class="input">
                <input type="text" name="companyname" id="companyname" required>
                </label>
                <span  id="usernameError"></span>
              </section> 

               <section class="col col-6">
                <label class="label">Password</label>
                <label class="input">
                  <input type="password" name="password" required />
                </label>
              </section>
            </div>

            <div class="row">
              <section class="col col-6">
                <label class="label">Contact E-mail</label>
				        <label class="input">
                  <input type="email" name="contactemail" required id="contactemail"/>
                </label>
              </section>
              <section class="col col-6">
                <label class="label">Contact No.</label>
              <label class="input">
                <input type="text" name="companynumber" id="companynumber" required>
              </label>
              </section>
            </div>

            <div class="row">
              <section class="col col-6">
                <label class="label">GST NO.</label>
                  <label class="input">
                     <input type="text" name="gstno" id="gstno" required>
                  </label>
                </section>
              <section class="col col-6">
                <label class="label">City</label>
      				  <label class="input">
                        <input type="text" name="city" id="city" required>
      				  </label>
              </section>
            </div>
            <div class="row">
              <section class="col col-6">
                <label class="label">State</label>
				        <label class="input">
                  <input type="text" name="state" id="state" required>
                </label>
            </section>
                <section class="col col-6">
                <label class="label">Country</label>
      				  <label class="input">
                  <input type="text" name="country" id="country" required>
      				  </label>
              </section>
            </div>
           
          </fieldset>
            <div class="form-group">
			   <div class="col-md-offset-4 col-md-4">
				   <button type="submit" id="btnInsert" class="btn btn-primary">
              {{ __('Start') }}
             </button>
			</div>
		  </div>
       
        </form>
        </div>
        
      </div>
      
      
</div><!-- end content area -->

<div class="clearfix marb12"></div>



@stop


