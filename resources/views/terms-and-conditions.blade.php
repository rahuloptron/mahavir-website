@extends('layouts.app')
@section('content')

<div class="clearfix margin_top10"></div>

<div class="page_title2">
<div class="container">
	
    <h1>Term of use and conditions </h1>

</div>
</div><!-- end page title --> 

<div class="clearfix"></div>
<div class="content_fullwidth less2">
<div class="container">

<div class="content">
<h3>Mahavir Consultants Term Of use</h3>
<div class="big_text1">Mahavir Consultants makes available the information,documents,software, and products,various services and subscriptions offered by development subject to terms and conditions accessing this Site, which includes your access to or use of any of the Services, you agree to the Terms of Use.This site will subject you and will give the information of the latest version of Terms of Use posted on this Site 
</div>
<div class="clearfix margin_top3"></div>
<h4>Ownership and Restrictions</h4> 
<p>Mahavir Consultantshas license or has the right to use and provide the Site and Materials on the Site.Indicated, all the Materials featured or displayed on the Site, including text, images, photographs, graphics, illustrations, layout of the Site, sound, software, trade dress, trademarks, patents, and the selection and arrangement</p>

<div class="clearfix margin_top3"></div>
<h4>Registration Data and Account Security </h4> 
<p>In consideration of your use of the Site, you agree to: </p>
<ul class="list1">
<li>(a) provide correct, accurate, current, and complete information about you as prompted by any registration forms on the Site ("Registration Data")</li>
<li>(b) help maintain accuracy of such data by promptly in a reasonable manner of such changes </li>
<li>(c) maintain the security of your password and login ID</li>
<li>(d) notify Mahavir Consultants immediately of any unauthorized use of your account or other breach of security</li>
<li>(e) accept all responsibility for any and all activity from your account</li>
<li>(f) accept all risks of unauthorized access to the Registration Data and any other information you provide to Mahavir Consultants. You are responsible for consequences of all uses of your account and identification information whether or not actually or expressly authorized by you. 
</li>
</ul>
<div class="clearfix margin_top3"></div>
<h4>Intellectual Property Rights</h4> 
<p>Mahavir Consultants s the sole owner or licensee of all the rights to the Site and the Contents except as where indicated otherwise. All Contents are the property of Mahavir Consultants its affiliates or third parties and  are protected by all the applicable laws, including, but not limited to copyright, trademark, trade-names, patents, Internet domain names, data protection, privacy and publicity rights and other similar rights and statutes. All title, ownership and intellectual property rights in the Site and the Content shall remain with Mahavir Consultants its affiliates or respective owner or licensor, as the case may be and does not pass on to you or your representatives unless specifically agreed to by relevant parties. </p>

<div class="clearfix margin_top3"></div>
<h4>Copyright</h4>
<p>Mahavir Consultants owns the copyright in the selection, coordination, arrangement and enhancement of the Site. The Site is protected by copyright as a collective work and compilation, pursuant to U.S. copyright laws, international conventions, and other copyright laws as applicable.Mahavir Consultants respects the intellectual property rights of others, and we expect our user(s) to do the same. </p> 
<div class="clearfix margin_top3"></div>
<h4>Trademarks</h4>
<p>
Mahavir Consultants  is the exclusive owner and holder of Mahavir Consultants rademarks, logos or service marks and including any other slogan(s) or design contained in the Site and otherwise used in its business activities its affiliates and subsidiaries, and may not be copied, imitated or used, in whole or in part, without the prior written permission of Mahavir Consultants. To the extent a Mark or logo does not appear in the aforementioned list or on the Site does not constitute a waiver of any and all intellectual property rights that Mahavir Consultants or its affiliates and subsidiaries have established in any of its product, feature, or service names or logos. 
</p><div class="clearfix margin_top3"></div>
<h4>Governing Law and Jurisdiction </h4>
<p>These Terms of Use shall be governed by and construed in accordance with the laws of the State of Texas, without regard to conflicts of laws provisions. The sole and exclusive jurisdiction for any action or proceeding arising out of or related to these Terms of Use shall be an appropriate State or Federal court located in Harris County in the State of Texas and you hereby irrevocably consent to the jurisdiction of such courts. 
</p>

<div class="clearfix margin_top3"></div>

<h4>General Provisions </h4>
<p> If one or more of the provisions contained in these Terms of Use is found to be invalid, illegal or enforceable in any respect, the validity, legality, and enforceability of the remaining provisions shall not be affected. Such provisions shall be revised only to the extent necessary to make them enforceable. <br>
These Terms of Use constitute the entire agreement between you and OSSCube with respect to the subject matter hereof, and supersede all previous written or oral agreements between the parties with respect to such subject matter. </p>

<div class="clearfix margin_top3"></div>
<h4>Your acceptance of these terms</h4>
<p>
By using this Site network of web sites, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes. If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.</p>
<div class="clearfix margin_top3"></div>
<h4>Contact Us</h4>
<p>If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:<br/>
<a href="/index.php">Mahavir Consultants</a><br>
<a href="http://mahavirconsultants.com">http://mahavirconsultants.com</a>

<div class="clearfix margin_top3"></div>
    
</div><!-- end content left side -->

</div>
</div><!-- end content area -->



@stop