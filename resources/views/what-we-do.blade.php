@extends('layouts.app')
@section('content')


<div class="clearfix"></div>

<div class="parallax_section8">
<div class="container">

    <h1 class="white">Worried about Scalability? Join US</h1>
    
</div>
</div><!-- end features section 11 -->

<div class="clearfix"></div>

<div class="features_sec7">
<div class="container">

    <div class="title2 sy2">
        <h2><span class="line"></span><span class="text">Expertise / What we do</span>   </h2>
    </div>
    
    <div class="clearfix margin_top2"></div>
    
    <div class="one_fourth">
        
        <div class="box">
        

            <div class="ibox"><img src="images/custom-develop.png" alt=""> <h6>Custom Web Applications</h6></div>

            
        </div>
        
    </div><!-- end section -->
    
    <div class="one_fourth">
        
        <div class="box">
        
            <div class="ibox"><img src="images/site-img21.png" alt=""> <h6>Mobile <br>Applications
</h6></div>
          
        </div>
        
    </div><!-- end section -->
    
    <div class="one_fourth">
        
        <div class="box">
        
            <div class="ibox"><img src="images/site-img20.png" alt=""> <h6>E.R.P.<br> MahaErp</h6></div>
            
        </div>
        
    </div><!-- end section -->
    
    <div class="one_fourth last">
        
        <div class="box">
        
            <div class="ibox"><img src="images/seo-tags-icon.png" alt=""> <h6>Digital<br> Marketing
</h6></div>

          
        </div>
        
    </div><!-- end section -->

</div>
</div>

<div class="clearfix"></div>

<div class="features_sec59">
<div class="container">
	
    <div class="one_full stcode_title9">
    
    	<h2>Featured Services
        <span class="line"></span></h2>

    </div>
    
    <div class="clearfix marb4"></div>
    
    <div>
    
            <div>
            
            	<div class="one_half"><img src="images/web-development.png" alt="" /></div>
                
                <div class="one_half last">
                
                	<h3 class="color">Custom Web Applications</h3>

                    <p>Customized Web-based solutions are changing businesses. with 24x7 over the internet secure applications are the trend for the years tocome. With our expert Architecting and Technology adoption using both new and legacy system combinations, We help you get your businesess requirements drive through the technolgoy bottlenecks like knife over butter.</p>
                     <ul class="list1 small">
                        <li><i class="fa fa-chevron-right"></i>Static & Dynamic Website Development</li>
                        <li><i class="fa fa-chevron-right"></i>Web Based Business and Workflow Applications</li>
						<li><i class="fa fa-chevron-right"></i>Data analysis and retargeting logic</li>
                    </ul>

                </div>
                
                <div class="clearfix margin_top10"></div>
                     
			</div><!-- end section -->
            
            <div>
            
            	<div class="one_half">
                 <h3 class="color">Mobile Applications (Native and Hybrid)</h3>
                   
                    <p>Mobile apps have become an integral part of personal as well as professional life. With BYOD making the major trend cross the industry Web or Desktop applications are taking a backseat for information specific applications</p>

                    <ul class="list1 small">

                        <li><i class="fa fa-chevron-right"></i>Android Java Native</li>
                        <li><i class="fa fa-chevron-right"></i>Firebase Communications as well as database</li>
                        <li><i class="fa fa-chevron-right"></i>iOS objective C++ and Swift or both</li>
                        <li><i class="fa fa-chevron-right"></i>Hybrid Cross platfrom applications</li>
                       <li><i class="fa fa-chevron-right"></i>Progressive Web Applications</li>
                    </ul>


                </div>
                
                <div class="one_half last"><img src="images/mobileappdevelopment.jpg" alt="" />
                
                	
                    
                </div>
                
                <div class="clearfix margin_top10"></div>
                     
			</div><!-- end section -->
            
            <div>
            
            	<div class="one_half"><img src="images/erp-integration.png" alt="" /></div>
                
                <div class="one_half last">
                
                	<h3 class="color">All-in-one ERP</h3>
                	
                    <p>MahaErp is a derivative Opensource Enterprise Grade OpenErp application fits requirements of almost every user of the business. It's Modular structure ensures zero over heads while ensuring integration and build as you Grow. YOu can only have CRM for sales or just your manufacturing services</p>
                    <ul class="list1 small">
                        <li><i class="fa fa-chevron-right"></i>C.R.M. (Lead, Sales, Services, Customer Support)</li>
                        <li><i class="fa fa-chevron-right"></i>Content Management System (CMS)</li>
                        <li><i class="fa fa-chevron-right"></i>Manufacturing and Human Resource</li>
                        <li><i class="fa fa-chevron-right"></i>Accounting and Finance</li>
                    </ul>
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
			</div><!-- end section -->
               
              
		</div>

</div>
</div><!-- end features section 59 -->

<div class="clearfix"></div>

<div class="features_sec49 two">
<div class="container">

    <h2>Need help? Ready to Help you with Whatever you Need</h2>
    
    <strong>+91 9869995871</strong>
    
</div>
</div>


<div class="clearfix"></div>

@stop




