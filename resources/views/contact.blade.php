@extends('layouts.app')
@section('content')


<div class="clearfix"></div>

<div class="page_title">
<div class="container">

    <div class="title"><h1>Contact Us</h1></div>
        
  
</div>
</div><!-- end page title -->

<div class="clearfix"></div>

<div class="content_fullwidth">

<div class="container">
	
		<div class="one_half">
      
        <p>Feel free to talk to our online representative at any time you please using our Live Chat system on our website or one of the below instant messaging programs.</p>
        <br />
        <p>Please be patient while waiting for response. (24/7 Support!) <strong>Phone General Inquiries: <a href="tel://+91-9869-995-871">+91-9869-995-871</a></strong></p>
        <br /><br />

        <div class="cforms">
		{!! Form::open(['url' => '/inquiry', 'class' => 'sky-form','id' => 'sky-form' ,'files' => true]) !!}
        
          <header>Contact <strong>Form</strong></header>
          <fieldset>
            <div class="row">
              <section class="col col-6">
                <label class="label">Name</label>
				  <label class="input"><i class="icon-append icon-user"></i>
                  <input type="text" name="contactName" id="name" required>
				  </label>
              </section>
              <section class="col col-6">
                <label class="label">E-mail</label>
				<label class="input"> <i class="icon-append icon-envelope-alt"></i>
                  <input type="email" name="contactEmail" required id="email"/>
                </label>
                
              </section>
            </div>
            <section>
              <label class="label">Phone</label>
			   <label class="input"> <i class="icon-append icon-tag"></i>
                <input type="text" name="contactMobile" id="subject" required>
              </label>
              
			  </label>
            </section>
            <section>
              <label class="label">Message</label>
			  <label class="textarea"> <i class="icon-append icon-comment"></i>
              <textarea rows="4" name="message1" id="message1" required></textarea>
			  </label>
            </section>
            <section>
              <label class="checkbox">
                <input type="checkbox" name="copy" id="copy">
                <i></i>Send a copy to my e-mail address</label>
            </section>
          </fieldset>
          <div class="form-group">
			<div class="col-md-offset-4 col-md-4">
				{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Submit', ['class' => 'btn btn-primary']) !!}
			</div>
		  </div>
       
        {!! Form::close() !!}
        </div>
        
      </div>
      
      <div class="one_half last">
      
        <div class="address_info">
        
          <h4>Company <strong>Address</strong></h4>
          <ul>
            <li> <strong>Mahavir Consultants</strong><br />
              3, Mahavir Nagar, Shankar Lane, Kandivali (W), Mumbai, 400067, India<br />
              Telephone: +1 9869995871<br />
              E-mail: <a href="mailto:info@mahavirconsultants.com">info@mahavirconsultants.com</a><br />
              Website: <a href="index.html">www.mahavirconsultants.com</a> </li>
          </ul>
        </div>
        <div class="clearfix"></div>
        <h4>Find the <strong>Address</strong></h4>
        <iframe class="google-map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/place/Mahavir+Consultants+LLP/@19.1985214,72.8401435,17z/data=!3m1!4b1!4m5!3m4!1s0x3be7b7bbc6a5c0e3:0x68d9ab1e19e55f1e!8m2!3d19.1985214!4d72.8423322?hl=en-IN" width="550" height="340" frameborder="0" style="border:0" allowfullscreen></iframe>
        <br />
        <small><a href="https://www.google.com/maps/place/Mahavir+Consultants+LLP/@19.1985214,72.8401435,17z/data=!3m1!4b1!4m5!3m4!1s0x3be7b7bbc6a5c0e3:0x68d9ab1e19e55f1e!8m2!3d19.1985214!4d72.8423322?hl=en-IN">View Larger Map</a></small> </div>

</div>
</div><!-- end content area -->

<div class="clearfix marb12"></div>

@stop


