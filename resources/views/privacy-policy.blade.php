@extends('layouts.app')
@section('content')

<div class="clearfix margin_top10"></div>

<div class="page_title2">
<div class="container">
	
    <h1>Privacy Policy</h1>

     
</div>
</div><!-- end page title --> 

<div class="clearfix"></div>
<div class="content_fullwidth less2">
<div class="container">

<div class="content">
<h3>Mahavir Consultants Privacy Policy</h3>

<div class="big_text1">This Privacy Policy governs the manner in which Mahavir Consultants collects, uses, maintains and discloses information collected from users (each, a "User") of the http://www.mahavirconsultants.com website ("Site") This privacy policy applies to the Site and all products and services offered by  Mahavir Consultants
</div>
<div class="clearfix margin_top3"></div>
<h4>Personal Identification Information</h4> 
<p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, subscribe to the newsletter, respond to a survey, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, phone number. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>

<div class="clearfix margin_top3"></div>
<h4>Non-Personal Identification Information</h4> 
<p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>
<div class="clearfix margin_top3"></div>
<h4>Web Browser cookies</h4> 
<p>Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>
<div class="clearfix margin_top3"></div>
<h3>How to use collected information</h3>
<h5> Mahavir Consultants may collect and use users personal information for the following purpose:</h5>
<ul class="list1">

<li><i class="fa fa-check-square"></i>To improve customer service.<br/>
Information you provide helps us respond to your customer service requests and support needs more efficiently.
</li>
<li><i class="fa fa-check-square"></i>To personalize user experience.<br/>
We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.
</li>
<li><i class="fa fa-check-square"></i> To improve our Site.<br/>
We may use feedback you provide to improve our products and services.
</li>
<li><i class="fa fa-check-square"></i>To process payments.<br/>
We may use the information Users provide about themselves when placing an order only to provide service to that order. We do not share this information with outside parties except to the extent necessary to provide the service.
</li>
<li><i class="fa fa-check-square"></i> To run a promotion, contest, survey or other Site feature<br/>
To send Users information they agreed to receive about topics we think will be of interest to them.
</li>
<li><i class="fa fa-check-square"></i> To send periodic emails.<br/>
We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email.
</li>
</ul>

<div class="clearfix margin_top3"></div>
<h4>How we protect your information</h4>
<p>
We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.
</p><div class="clearfix margin_top3"></div>
<h4>Sharing Your personal Information</h4>
<p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</p>

<div class="clearfix margin_top3"></div>

<h4>Chnages to privacy policy</h4>
<p>Mahavir Consultants as the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page and send you an email. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>

<div class="clearfix margin_top3"></div>
<h4>Your acceptance of these terms</h4>
<p>
By using this Site network of web sites, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes. If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.</p>
<div class="clearfix margin_top3"></div>
<h4>Contact Us</h4>
<p>If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:<br/>
<a href="/index.php">Mahavir Consultants</a><br>
<a href="http://mahavirconsultants.com">http://mahavirconsultants.com</a>


 <div class="clearfix margin_top3"></div>
    
</div><!-- end content left side -->

<div>
	
</div>



@stop