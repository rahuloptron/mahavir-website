<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

<div class="container">


<hr>

{!! Form::open(['url' => '/inquiry', 'class' => 'form-horizontal', 'files' => true]) !!}

<div class="box-body">
<div class="form-group {{ $errors->has('contactName') ? 'has-error' : ''}}">
    {!! Form::label('contactName', 'Enter Contact Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contactName', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('contactName', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('contactEmail') ? 'has-error' : ''}}">
    {!! Form::label('contactEmail', 'Enter Contact Email', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contactEmail', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('contactEmail', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('contactMobile') ? 'has-error' : ''}}">
    {!! Form::label('contactMobile', 'Enter Contact Mobile', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contactMobile', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('contactMobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>


 <div class="form-group">

  {{Form::submit('Inquiry Now',array('class' => 'btn btn-primary btn-sm'))}} 

 </div>

{{Form::close()}} 

</div>

</body>
</html>
