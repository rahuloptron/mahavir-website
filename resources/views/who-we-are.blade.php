@extends('layouts.app')
@section('content')

<div class="clearfix"></div>

<div class="parallax_section8">
<div class="container">
    <h1 class="white">Emerging Tech-savvy partners
</h1>
    <h4 class="white">With our Tech-Savvy Team, our partners dont have to worry about thier IT needs</h4>
</div>
</div><!-- end features section 11 -->

<div class="clearfix"></div>

<div class="features_sec37">
<div class="container">
    
    <div class="stcode_title11">
        <h2>Our Purpose<em>Our Clients are our growth Partners, We grow as our partners grow, We at Mahavir Consultants make technology as enabler to take your business to next level by Merging the Two. We work along-side our Partners to ensure Technology enables their growth to fast track</em>
       </h2>
    </div>
    
</div>
</div>

<div class="clearfix"></div>

<div class="features_sec49 three" style="text-align: center">
<div class="container">

    <h2>Need help? Ready to Help you with Whatever you Need</h2>
    
    <strong><a href="tel://+91-9869-995-871">+91-9869-995-871</a></strong>
    
</div>
</div>


<div class="clearfix"></div>

@stop




