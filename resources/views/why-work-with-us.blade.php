@extends('layouts.app')
@section('content')
<div class="clearfix"></div>

<div class="parallax_section8">
<div class="container">
    <h1 class="white">Why companies choose Mahavir Consultants?</h1>
	
    <h4 class="white">We are focused on enabling companies to accelerate software development with exceptional developers. Our dedicated, non-freelance development resources and “Right the 1st Time” delivery model, remove road blocks related to software development.</h4>
</div>
</div><!-- end features section 11 -->
<div class="clearfix"></div>

<div class="features_sec37">
<div class="container">
    
    <div class="stcode_title11">
    
        <h2>Why Us<em>Top developers are in high demand, and everyone is tapping into a small pool of expert resources. They are hard to find and even harder to retain - not to mention escalating salaries. In all this, there is a significant impact on your product and delay in time-to-market. At Clarion you get freedom from all these obstacles and many more when looking to hire developers. Here is what makes Clarion the right fit.</em>
       </h2>
    
    </div>
    
</div>
</div>

<div class="clearfix"></div>

<div class="features_sec59">
<div class="container">
	
    <div class="one_full stcode_title9">
    
    	<h2>Featured Services
        <span class="line"></span></h2>

    </div>
    
    <div class="clearfix marb4"></div>
    
    <div>
    
            <div>
            
            	<div class="one_half"><img src="http://placehold.it/552x300" alt="" /></div>
                
                <div class="one_half last">
                
                	<h3 class="color">Several Design Options</h3>
                	
                    <h6 class="gray">Many desktop publishing packages and web page editors now use as their default model text, and a search.</h6>
                    
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    
                </div>
                
                <div class="clearfix margin_top10"></div>
                     
			</div><!-- end section -->
            
            <div>
            
            	<div class="one_half">
                 <h3 class="color">Clean &amp; Modern Design</h3>
                    
                    <h6 class="gray">Many desktop publishing packages and web page editors now use as their default model text, and a search.</h6>
                    
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

                </div>
                
                <div class="one_half last"><img src="http://placehold.it/552x300" alt="" />
                
                	
                    
                </div>
                
                <div class="clearfix margin_top10"></div>
                     
			</div><!-- end section -->
            
            <div>
            
            	<div class="one_half"><img src="http://placehold.it/552x300" alt="" /></div>
                
                <div class="one_half last">
                
                	<h3 class="color">Build Your Owne Website</h3>
                	
                    <h6 class="gray">Many desktop publishing packages and web page editors now use as their default model text, and a search.</h6>
                    
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
			</div><!-- end section -->
               
              
		</div>

</div>
</div><!-- end features section 59 -->

<div class="clearfix"></div>

<div class="features_sec49 two">
<div class="container">

    <h2>Need help? Ready to Help you with Whatever you Need</h2>
    
    <strong>+91 9876543210</strong>
    
</div>
</div>


<div class="clearfix"></div>

@stop




