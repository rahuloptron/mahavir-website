<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('table', function () {
    return view('table');
});

Route::get('/', function () {
    return view('index', ['title' => 'Mahavir consultants']);
});

Route::get('/index.html', function () {
    return view('index', ['title' => 'Mahavir consultants']);
});

Route::get('/contact.html', function () {
    return view('contact', ['title' => 'Contact Mahavir consultants']);
});

Route::get('/what-we-do.html', function () {
    return view('what-we-do', ['title' => 'What we do']);
});


Route::get('/final.html', function () {
    return view('final', ['title' => 'final']);
});

Route::get('/thank-you.html', function () {
    return view('thank-you', ['title' => 'Thank You']);
});

Route::get('/which-technologies.html', 'TechController@index');

Route::get('/who-we-are.html', function () {
    return view('who-we-are', ['title' => 'Who we are']);
});


Route::get('/how-we-do-it.html', function () {
    return view('how-we-do-it', ['title' => 'How we do']);
});

Route::get('/terms.html', function () {
    return view('terms-and-conditions', ['title' => 'Terms and Conditions - Mahavir Consultants']);
});


Route::get('/privacy-policy.html', function () {
    return view('privacy-policy', ['title' => 'Privacy Policy - Mahavir Consultants']);
});



Route::post('/send','FormController@send');

//Route::resource('/index2/{id}','indexController');

Route::resource('/create','CreateBlogController');
Route::get('/blog/{id}','CreateBlogController@show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin', 'Admin\AdminController@index');
Route::resource('admin/roles', 'Admin\RolesController');
Route::resource('admin/permissions', 'Admin\PermissionsController');
Route::resource('admin/users', 'Admin\UsersController');
Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);


/**
 * Catchall route
 */
/*

  Route::get('{view}', function ($view) {
    try {
      return view($view);
    } catch (\Exception $e) {
      abort(404);
    }
  })->where('view', '.*');*/

Route::resource('products', 'productsController');
Route::resource('categories', 'categoriesController');

Route::get('/listing', 'productsController@productShowing');
Route::get('/final/{id}', 'productsController@productFinal');

Route::resource('inquiry', 'inquiryController');

Route::get('crm', 'crmController@index');
Route::post('crm', 'crmController@store');