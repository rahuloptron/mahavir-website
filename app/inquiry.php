<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inquiry extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inquiries';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['contactName', 'contactEmail', 'contactMobile', 'productName', 'productId'];

    
}
