<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\inquiry;
use Illuminate\Http\Request;

class inquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
       $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $inquiry = inquiry::where('contactName', 'LIKE', "%$keyword%")
                ->orWhere('contactEmail', 'LIKE', "%$keyword%")
                ->orWhere('contactMobile', 'LIKE', "%$keyword%")
                ->orWhere('productName', 'LIKE', "%$keyword%")
                ->orWhere('productId', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $inquiry = inquiry::paginate($perPage);
        }

        return view('inquiry.index', compact('inquiry'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $odoo = new \Edujugon\Laradoo\Odoo();

        $product_list = $odoo->fields('name')->get('crm.lead');

        return view('inquiry.create', compact('product_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $odoo = new \Edujugon\Laradoo\Odoo();

        $this->validate($request, [
			'contactName' => 'required',
			'contactEmail' => 'required',
			'contactMobile' => 'required'
		]);


        $contactName = $request->contactName;
        $contactEmail = $request->contactEmail;
        $contactMobile = $request->contactMobile;   
        $message1 = $request->message1;   

      
       $id = $odoo->create('crm.lead',['name' => $message1,
                                        'contact_name' => $contactName,
                                        'email_from' => $contactEmail,
                                        'phone' => $contactMobile,
                                        ]);

       
       return redirect('thank-you.html')->with('flash_message', 'Inquiry added!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $inquiry = inquiry::findOrFail($id);

        return view('inquiry.show', compact('inquiry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $inquiry = inquiry::findOrFail($id);

        return view('inquiry.edit', compact('inquiry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'contactName' => 'required',
			'contactEmail' => 'required',
			'contactMobile' => 'required'
		]);
        $requestData = $request->all();
        
        $inquiry = inquiry::findOrFail($id);
        $inquiry->update($requestData);

        return redirect('inquiry')->with('flash_message', 'inquiry updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        inquiry::destroy($id);

        return redirect('inquiry')->with('flash_message', 'inquiry deleted!');
    }

   
}
