<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
class TechController extends Controller
{
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		$dir_list = File::glob('images/technologies/*', GLOB_NOSORT);
		if ($dir_list === false)
		{
			die("An error occurred.");
		}
		// return $dir_list; 
        return view('which-technologies',['title' => 'Technologies'])->with('dir_list' ,$dir_list);
    }
}
