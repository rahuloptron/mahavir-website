<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\product;
use Illuminate\Http\Request;

class productsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $products = product::where('product_id', 'LIKE', "%$keyword%")
                ->orWhere('productName', 'LIKE', "%$keyword%")
                ->orWhere('productCategory', 'LIKE', "%$keyword%")
                ->orWhere('productPrice', 'LIKE', "%$keyword%")
                ->orWhere('productLocation', 'LIKE', "%$keyword%")
                ->orWhere('productModel', 'LIKE', "%$keyword%")
                ->orWhere('productDesc', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $products = product::paginate($perPage);
        }

        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'productName' => 'required',
             'productImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

        $requestData = $request->all();

        $file = $request->file('productImage') ;

        $input['imagename'] = time().'.'.$file->getClientOriginalExtension();

        $destinationPath = public_path().'/images/productImage' ;

        $file->move($destinationPath,$input['imagename']);

        $requestData['productImage'] = $input['imagename'];

        
        product::create($requestData);

        return redirect('products')->with('flash_message', 'product added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $product = product::findOrFail($id);

        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $product = product::findOrFail($id);

        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'product_id' => 'required',
			'productName' => 'required'
		]);
        $requestData = $request->all();
        
        $product = product::findOrFail($id);
        $product->update($requestData);

        return redirect('products')->with('flash_message', 'product updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        product::destroy($id);

        return redirect('products')->with('flash_message', 'product deleted!');
    }

    public function productShowing(Request $request)
    {
        $products = product::get();

        return view('listing', compact('products'));
    }

    public function productFinal($id)
    {
        $products = product::where('id',$id)->get();

        return view('final', compact('products'));
    }
}
