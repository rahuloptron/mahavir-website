<?php

namespace App\Http\Controllers;
use App\Mail\SendMail;
use Illuminate\Http\Request;
use Mail;

class FormController extends Controller
{


    public function send(Request $request)
    {

    	

    	$name = $request->contactName; 
    	$email = $request->contactEmail; 
    	$mobile = $request->contactMobile; 
    	$message1 = $request->message1; 



		$data = array(
	    		'name' => $name,
			   	'mobile' => $mobile,
			    'email' => $email,
			    'message1' => $message1
    	);
        
		Mail::send('mail-template', $data, function ($message) 
        {
           // $to =  config('app.enq_mail_to_addr');

        	$to = 'rahul@optroninfo.com';
     
    	   $message->to($to)->subject('New Enquiry Generated');

    	});

		return redirect('thank-you.html') ;

    	//Mail::to('rahul@optroninfo.com')->send(new SendMail());
    }
}
