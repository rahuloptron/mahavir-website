<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\companyUser;
use App\company;


class crmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companyArray = company::get()->pluck('companyname');

        $companyArray1 =  iterator_to_array($companyArray);

        $companyRecord = array_map('strtolower', $companyArray1);

       return view('crm')->with('companyRecord', $companyRecord);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $user = 'admin';
            $password = $request->password;
            $dbname = $request->subdomain;
            $lang="en_US";
            $master_password = "nehal3110";

            $odoo = new \Edujugon\Laradoo\Odoo();

            $client = $odoo->getClient($odoo->getHost(). "/xmlrpc/db");

            $response = $client->create_database($master_password, $dbname, $user, $lang, $password);
           
            $user = new companyUser([

            'username' => 'admin',
            'password' => $request->get('password'),
            'companynumber' => $request->get('companynumber'),
            'contactemail' => $request->get('contactemail')
             
            ]);

            $user->save();

            $company = new company([
            'userid' => $user->id,
            'companyname' => $request->get('companyname'),
            'city' => $request->get('city'),
            'state' => $request->get('state'),
            'country' => $request->get('country'),
            'subdomain' => $request->get('subdomain'),
            'domain' => 'mahavirconsultants.com',
            'gstno' => $request->get('gstno')
            ]);

            $company->save();

          
        return back()->with('status', 'CRM Service Started Successfully! Thank You');
  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
