<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Jenssegers\Mongodb\Auth\company as Authenticatable;


class crm extends Model
{
    protected $connection = 'mongodb';
    //
    protected $table = 'company';
    //
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'companyname','address', 'companynumber','contactemail','subdomain','username','password','domain','gstno',
    ];
}
