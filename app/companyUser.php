<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Jenssegers\Mongodb\Auth\company as Authenticatable;

class companyUser extends Model
{
    protected $connection = 'mongodb';
    //
    protected $table = 'companyUser';
    //
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'companynumber','contactemail','username','password',
    ];
}
